package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberFormatTest {

    private static Locale INDONESIA;
    private static Locale USA;
    private static double decimalInDouble;
    private static String decimalInString;

    @BeforeAll
    static void beforeAll() {
        INDONESIA = Locale.of("id", "ID");
        USA = Locale.US;

        decimalInDouble = 1_000_000.255;
        decimalInString = "1,000,000.255";
    }

    @Test
    void testNumberFormat() {
        var numberFormat = NumberFormat.getInstance();
        var format = numberFormat.format(decimalInDouble);

        System.out.printf("Before format = %f\nAfter format = %s", decimalInDouble, format);

        assertEquals(decimalInString, format);
    }

    @Test
    void testNumberFormatWithLocale() {
        var numberFormat = NumberFormat.getInstance(INDONESIA);
        var format = numberFormat.format(decimalInDouble);

        System.out.printf("Before format = %f\nAfter format = %s", decimalInDouble, format);

        assertEquals("1.000.000,255", format);
    }

    @Test
    void testParseNumberFormatWithLocale() {
        var numberFormat = NumberFormat.getInstance(USA);

        try {
            double decimalParse = numberFormat.parse(decimalInString).doubleValue();

            System.out.printf("Before parse = %s\nAfter parse = %f", decimalInString, decimalParse);

            assertEquals(decimalParse, decimalInDouble);
        } catch (ParseException e) {
            System.out.println("Error parse : " + e.getMessage());
        }
    }
}
