package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;

class ResourceBundleTest {

    private static Locale INDONESIA;
    private static Locale SOUTH_KOREA;

    @BeforeAll
    static void beforeAll() {
        INDONESIA = Locale.of("id", "ID");
        SOUTH_KOREA = Locale.KOREA;
    }

    @Test
    void testResourceBundle() {
        var bundle = ResourceBundle.getBundle("message");

        System.out.println("Bundle get welcome = " + bundle.getString("welcome"));
        System.out.println("Bundle get hello = " + bundle.getString("hello"));
        System.out.println("Bundle get goodbye = " + bundle.getString("goodbye"));

        assertEquals("Welcome", bundle.getString("welcome"));
        assertEquals("Hello", bundle.getString("hello"));
        assertEquals("Goodbye", bundle.getString("goodbye"));
    }

    @Test
    void testResourceBundleWithLocale() {
        var bundle = ResourceBundle.getBundle("message", INDONESIA);

        System.out.println("Bundle get welcome = " + bundle.getString("welcome"));
        System.out.println("Bundle get hello = " + bundle.getString("hello"));
        System.out.println("Bundle get goodbye = " + bundle.getString("goodbye"));

        assertEquals("Selamat Datang", bundle.getString("welcome"));
        assertEquals("Halo", bundle.getString("hello"));
        assertEquals("Selamat Tinggal", bundle.getString("goodbye"));
    }

    @Test
    void testResourceBundleObject() {
        var bundle1 = ResourceBundle.getBundle("message", SOUTH_KOREA);
        var bundle2 = ResourceBundle.getBundle("message", SOUTH_KOREA);

        assertSame(bundle1, bundle2);
    }
}
