package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.MessageFormat;
import java.time.*;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageFormatTest {

    private static Locale INDONESIA;

    @BeforeAll
    static void beforeAll() {
        INDONESIA = Locale.of("id", "ID");
    }

    @Test
    void testMessageFormat() {
        var pattern = "Hi {0}, Anda bisa mencari data Anda dengan mengetikkan \"{0}\" pada halaman pencarian";
        var message = new MessageFormat(pattern);

        var format = message.format(new Object[]{
                "Willi"
        });

        System.out.println(format);
        assertEquals("Hi Willi, Anda bisa mencari data Anda dengan mengetikkan \"Willi\" pada halaman pencarian", format);
    }

    @Test
    void testMessageFormatWithResourceBundle() {
        var resourceBundle = ResourceBundle.getBundle("message");

        var pattern = resourceBundle.getString("welcome.message");

        var message = new MessageFormat(pattern);
        var format = message.format(new Object[]{
                "Willi", "Java I18N"
        });

        System.out.println(format);
        assertEquals("Hello Willi, Welcome to Java I18N", format);
    }

    @Test
    void testMessageFormatWithResourceBundleAndLocale() {
        var resourceBundle = ResourceBundle.getBundle("message", INDONESIA);

        var pattern = resourceBundle.getString("welcome.message");

        var message = new MessageFormat(pattern);
        var format = message.format(new Object[]{
                "Willi", "Java I18N"
        });

        System.out.println(format);
        assertEquals("Halo Willi, Selamat Datang di Java I18N", format);
    }

    @Test
    void testMessageFormatTypeWithResourceBundle() {
        var resourceBundle = ResourceBundle.getBundle("message", INDONESIA);

        var pattern = resourceBundle.getString("status.balance");

        var message = new MessageFormat(pattern, INDONESIA);

        // NOTE: java.util.Date
        var format1 = message.format(new Object[]{
                "Willi", new Date(), 10000000
        });

        System.out.println(format1);

        assertEquals("Halo Willi, Sekarang Kamis, 28 Desember 2023, saldo Anda adalah Rp10.000.000,00", format1);

        // NOTE: java.time
//        var zonedDateTime = ZonedDateTime.now();
        var zonedDateTime = LocalDateTime.of(LocalDate.parse("2023-12-28"), LocalTime.now()).atZone(ZoneId.systemDefault());
        var format2 = message.format(new Object[]{
                "Willi", Date.from(zonedDateTime.toInstant()), 10000000
        });

        System.out.println(format2);

        assertEquals("Halo Willi, Sekarang Kamis, 28 Desember 2023, saldo Anda adalah Rp10.000.000,00", format2);
    }

    @Test
    void testChoiceWithMessageFormatType() {
        var resourceBundle = ResourceBundle.getBundle("message", INDONESIA);

        var pattern = resourceBundle.getString("balance");

        var message = new MessageFormat(pattern, INDONESIA);
        var format = message.format(new Object[]{
                -10000000
        });

        System.out.println(format);

        assertEquals("Total Saldo = -Rp10.000.000,00 Hutang", format);
    }
}
