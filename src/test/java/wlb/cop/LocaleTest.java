package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocaleTest {

    private static Locale INDONESIAN_INDONESIA;

    @BeforeAll
    static void beforeAll() {
        Locale INDONESIAN_INDONESIA_OF = Locale.of("id", "ID");
        Locale INDONESIAN_INDONESIA_BUILDER = new Locale.Builder()
                .setLanguage("id")
                .setRegion("ID")
                .build();
        Locale INDONESIAN_INDONESIA_TAG = Locale.forLanguageTag("id-ID");

        INDONESIAN_INDONESIA = INDONESIAN_INDONESIA_OF;
    }

    @Test
    void testLocale() {
        var language = "id";
        var country = "ID";

        var locale = new Locale(language, country);

        System.out.println("Locale get Language = " + locale.getLanguage());
        System.out.println("Locale get Country = " + locale.getCountry());
        System.out.println("Locale display Language = " + locale.getDisplayLanguage());
        System.out.println("Locale display Country = " + locale.getDisplayCountry());

        assertEquals("id", locale.getLanguage());
        assertEquals("ID", locale.getCountry());
        assertEquals("Indonesian", locale.getDisplayLanguage());
        assertEquals("Indonesia", locale.getDisplayCountry());
    }

    @Test
    void testNewLocaleDefault() {
        Locale EN_UK_DEFAULT = Locale.UK;

        System.out.println("Locale get Language = " + EN_UK_DEFAULT.getLanguage());
        System.out.println("Locale get Country = " + EN_UK_DEFAULT.getCountry());
        System.out.println("Locale display Language = " + EN_UK_DEFAULT.getDisplayLanguage());
        System.out.println("Locale display Country = " + EN_UK_DEFAULT.getDisplayCountry());

        assertEquals("en", EN_UK_DEFAULT.getLanguage());
        assertEquals("GB", EN_UK_DEFAULT.getCountry());
        assertEquals("English", EN_UK_DEFAULT.getDisplayLanguage());
        assertEquals("United Kingdom", EN_UK_DEFAULT.getDisplayCountry());
    }

    @Test
    void testNewLocaleCreate() {
        System.out.println("Locale get Language = " + INDONESIAN_INDONESIA.getLanguage());
        System.out.println("Locale get Country = " + INDONESIAN_INDONESIA.getCountry());
        System.out.println("Locale display Language = " + INDONESIAN_INDONESIA.getDisplayLanguage());
        System.out.println("Locale display Country = " + INDONESIAN_INDONESIA.getDisplayCountry());

        assertEquals("id", INDONESIAN_INDONESIA.getLanguage());
        assertEquals("ID", INDONESIAN_INDONESIA.getCountry());
        assertEquals("Indonesian", INDONESIAN_INDONESIA.getDisplayLanguage());
        assertEquals("Indonesia", INDONESIAN_INDONESIA.getDisplayCountry());
    }
}
