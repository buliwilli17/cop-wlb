package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DateTimeFormatTest {

    private static Locale INDONESIA;
    private static Locale JAPAN;

    @BeforeAll
    static void beforeAll() {
        INDONESIA = Locale.of("id", "ID");
        JAPAN = Locale.JAPAN;
    }

    // NOTE: DateFormat
    @Test
    void testDateFormat() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = new SimpleDateFormat(pattern);
        var dateFormat = formatter.format(new Date());

        System.out.println(dateFormat);

        assertEquals("Thursday, 28 December 2023", dateFormat);
    }

    @Test
    void testDateFormatWithLocale() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = new SimpleDateFormat(pattern, INDONESIA);
        var dateFormat = formatter.format(new Date());

        System.out.println(dateFormat);

        assertEquals("Kamis, 28 Desember 2023", dateFormat);
    }

    @Test
    void testParseDateFormatWithLocale() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = new SimpleDateFormat(pattern, JAPAN);

        try {
            var dateFormat = formatter.parse("木曜日, 28 12月 2023");

            System.out.println(dateFormat);

            assertEquals(new Date().getDate(), dateFormat.getDate());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }


    // NOTE: DateTimeFormatter
    @Test
    void testDateTimeFormatter() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = DateTimeFormatter.ofPattern(pattern);
        var dateFormat = LocalDate.now().format(formatter);

        System.out.println(dateFormat);

        assertEquals("Thursday, 28 December 2023", dateFormat);
    }

    @Test
    void testDateTimeFormatterWithLocale() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = DateTimeFormatter.ofPattern(pattern, INDONESIA);
        var dateFormat = LocalDate.now().format(formatter);

        System.out.println(dateFormat);

        assertEquals("Kamis, 28 Desember 2023", dateFormat);
    }

    @Test
    void testParseDateTimeFormatterWithLocale() {
        var pattern = "EEEE, dd MMMM yyyy";
        var formatter = DateTimeFormatter.ofPattern(pattern, JAPAN);
        var dateFormat = LocalDate.parse("木曜日, 28 12月 2023", formatter);

        System.out.println(dateFormat);

        assertEquals(LocalDate.now(), dateFormat);
    }
}
