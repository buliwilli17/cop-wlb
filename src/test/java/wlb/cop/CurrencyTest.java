package wlb.cop;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CurrencyTest {

    private static Locale JAPAN;
    private static Locale USA;
    private static long moneyInLong;
    private static String moneyInString;

    @BeforeAll
    static void beforeAll() {
        JAPAN = Locale.JAPAN;
        USA = Locale.US;

        moneyInLong = 1_000_000;
        moneyInString = "￥1,000,000";
    }

    @Test
    void testCurrency() {
        var currency = Currency.getInstance(USA);

        System.out.println("Currency get Display Name = " + currency.getDisplayName());
        System.out.println("Currency get Currency Code = " + currency.getCurrencyCode());
        System.out.println("Currency get Symbol = " + currency.getSymbol());

        assertEquals("US Dollar", currency.getDisplayName());
        assertEquals("USD", currency.getCurrencyCode());
        assertEquals("$", currency.getSymbol());
    }

    @Test
    void testCurrencyInNumberFormat() {
        var numberFormat = NumberFormat.getCurrencyInstance(JAPAN);
        var moneyFormat = numberFormat.format(moneyInLong);

        System.out.printf("Before format = %d\nAfter format = %s", moneyInLong, moneyFormat);

        assertEquals(moneyInString, moneyFormat);
    }

    @Test
    void testParseCurrencyInNumberFormat() {
        var numberFormat = NumberFormat.getCurrencyInstance(JAPAN);

        try {
            long moneyParse = numberFormat.parse(moneyInString).intValue();

            System.out.printf("Before parse = %s\nAfter parse = %d", moneyInString, moneyParse);

            assertEquals(moneyInLong, moneyParse);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }
}
